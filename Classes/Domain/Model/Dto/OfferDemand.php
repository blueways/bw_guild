<?php

namespace Blueways\BwGuild\Domain\Model\Dto;

/**
 * Class OfferDemand
 *
 * @package Blueways\BwGuild\Domain\Model\Dto
 */
class OfferDemand extends BaseDemand
{

    public CONST TABLE = 'tx_bwguild_domain_model_offer';
}
